;; game state memory location
  .equ T_X, 0x1000                  ; falling tetrominoe position on x
  .equ T_Y, 0x1004                  ; falling tetrominoe position on y
  .equ T_type, 0x1008               ; falling tetrominoe type
  .equ T_orientation, 0x100C        ; falling tetrominoe orientation
  .equ SCORE,  0x1010               ; score
  .equ GSA, 0x1014                  ; Game State Array starting address
  .equ SEVEN_SEGS, 0x1198           ; 7-segment display addresses
  .equ LEDS, 0x2000                 ; LED address
  .equ RANDOM_NUM, 0x2010           ; Random number generator address
  .equ BUTTONS, 0x2030              ; Buttons addresses

  ;; type enumeration
  .equ C, 0x00
  .equ B, 0x01
  .equ T, 0x02
  .equ S, 0x03
  .equ L, 0x04

  ;; GSA type
  .equ NOTHING, 0x0
  .equ PLACED, 0x1
  .equ FALLING, 0x2

  ;; orientation enumeration
  .equ N, 0
  .equ E, 1
  .equ So, 2
  .equ W, 3
  .equ ORIENTATION_END, 4

  ;; collision boundaries
  .equ COL_X, 4
  .equ COL_Y, 3

  ;; Rotation enumeration
  .equ CLOCKWISE, 0
  .equ COUNTERCLOCKWISE, 1

  ;; Button enumeration
  .equ moveL, 0x01
  .equ rotL, 0x02
  .equ reset, 0x04
  .equ rotR, 0x08
  .equ moveR, 0x10
  .equ moveD, 0x20

  ;; Collision return ENUM
  .equ W_COL, 0
  .equ E_COL, 1
  .equ So_COL, 2
  .equ OVERLAP, 3
  .equ NONE, 4

  ;; start location
  .equ START_X, 6
  .equ START_Y, 1

  ;; game rate of tetrominoe falling down (in terms of game loop iteration)
  .equ RATE, 5

  ;; standard limits
  .equ X_LIMIT, 12
  .equ Y_LIMIT, 8


  ;; TODO Insert your code here

;BEGIN:main
main:
	
	addi sp, zero, 0x1820
	addi t0, zero, 6
	call reset_game

loop1:
	br 40						;loop2
	addi a0, zero, PLACED
	call draw_tetromino
	br 124						;scnd_while_loop
	call generate_tetromino
	addi a0, zero, OVERLAP
	call detect_collision
	beq v0, a0, -40				;main
	addi a0, zero, FALLING
	call draw_tetromino
	br -44						;loop1

loop2:
	add t1, zero, zero
	addi t0, zero, 6
	br	32						;while_loop
	addi a0, zero, NOTHING
	call draw_tetromino
	addi a0, zero, moveD
	call act
	addi a0, zero, FALLING
	call draw_tetromino
	bne v0, zero, -80		    	;retourne loop1
	br -44				    	;loop2


while_loop: 
	call draw_gsa
	call display_score
	call wait
	addi a0, zero, NOTHING
	call draw_tetromino
	call get_input
	add a0, zero, v0
	call act
	addi a0, zero, FALLING
	call draw_tetromino
	addi t1, t1, 1
	blt t1, t0, -48			;while_loop
	br -84					;retourne  dans loop2

scnd_while_loop:
	call detect_full_line
	addi t2, zero, 8
	beq v0, t2, -136			;return_in_loop1
	add a0, zero, v0
	call remove_full_line
	call increment_score
	call display_score
	br -32 						;scnd_while_loop


;END:main

; BEGIN:clear_leds
clear_leds:
	addi sp, sp, -4
	stw t0, 0(sp)
    stw zero, LEDS(zero)
	addi t0, zero, 4
    stw zero, LEDS(t0)
	add t0, t0, t0
    stw zero, LEDS(t0)
	ldw t0, 0(sp)
	addi sp, sp, 4
    ret
; END:clear_leds


; BEGIN:set_pixel
set_pixel:
	addi sp, sp, -24
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw t2, 8(sp)
	stw t3, 12(sp)
	stw t4, 16(sp)
	stw t5, 20(sp)
    cmpgeui t0, a0, 8 ;check si >= 8
    cmpgeui t1, a0, 4 ;check si >= 4
    addi t2, zero, 1 ;cte 1
    beq t0, t2, 68
    beq t1, t2, 28
;Cas 1    
    ldw t4, 0x2000(zero)
    slli t3, a0, 3
    add t3, t3, a1
    sll t5, t2, t3
    or t4, t4, t5
    stw t4, 0x2000(zero)
	br 32
;Cas 2
    ldw t4, 0x2004(zero)
    addi t5, zero, 4 ; cte = 4
    sub t3, a0, t5
    slli t3, t3, 3
    add t3, t3, a1
    sll t5, t2, t3
    or t4, t4, t5
    stw t4, 0x2004(zero)
	br 32
; Cas3
    ldw t4, 0x2008(zero)
    addi t5, zero, 8 ; cte = 8
    sub t3, a0, t5
    slli t3, t3, 3
    add t3, t3, a1
    sll t5, t2, t3
    or t4, t4, t5
    stw t4, 0x2008(zero)

	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	ldw t3, 12(sp)
	ldw t4, 16(sp)
	ldw t5, 20(sp)
	addi sp, sp, 24
    ret
; END:set_pixel


;BEGIN:wait
wait:
	;ret ;HIWJOKPWKPSWJDIWHDOWKOSWKOJNWIDIDHIOWSJWOIDOWHDI
	addi sp, sp, -12
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw t2, 8(sp)

    addi t1, zero, 1
    slli t0, t1, 19
	slli t2, t1, 18
	add t0, t0, t2
    beq t0, zero, 8
    sub t0, t0, t1
	br -12

	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	addi sp, sp, 12
    ret
;END:wait


;BEGIN:get_input
get_input:
	;add v0, zero, zero
	;ret ;aoijIOHEDIOQHDHOEDJOWIHDOIWHEDOIWHDOIHEDOIWHEDIOWHEDIOWHEDO
	addi sp, sp, -20
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw t2, 8(sp)
	stw t3, 12(sp)
	stw ra, 16(sp)

	Add t0, zero, zero ;compteur 
	Add t1, zero, zero
	ldw t2, BUTTONS+4(zero) 
	Add v0, zero, zero
	addi t3, zero, 1
	beq t2, zero, 96

loopiloopi:
	and t1, t2, t3
	blt zero, t1, 12 ;decodeur
	srl t2, t2, t3
	addi t0, t0, 1
	br -20 ;loopiloopi

decodeur:
	add t3, zero, zero
	beq t0, t3, 32 	;cas 1

	addi t3, t3, 1
	beq t0, t3, 32	;cas 2

	addi t3, t3, 1
	beq t0, t3, 32	;cas 3

	addi t3, t3, 1
	beq t0, t3, 32	;cas 4

	addi t3, t3, 1
	Beq t0, t3, 32	;cas 5

cas_1:
	addi v0, zero, 0x01
	br 28		;fin
cas_2:
	addi v0, zero, 0x02
	br 20		;fin
cas_3:
	addi v0, zero, 0x04
	br 12		;fin
cas_4:
	addi v0, zero, 0x08
	br 4		;fin
cas_5:
	addi v0, zero, 0x10

fin:
	stw zero, BUTTONS+4(zero)
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	ldw t3, 12(sp)
	ldw ra, 16(sp)
	addi sp, sp, 20
	ret
;END:get_input



;BEGIN:set_gsa
set_gsa:
	addi sp, sp, -4
	stw t0, 0(sp)
	slli t0, a0, 3
	add t0, t0, a1
	slli t0, t0, 2
	stw a2, GSA(t0)
	ldw t0, 0(sp)
	addi sp, sp, 4
    ret
;END:set_gsa

;BEGIN:get_gsa
get_gsa:
	addi sp, sp, -4
	stw t0, 0(sp)
	slli t0, a0, 3
	add t0, t0, a1
	slli t0, t0, 2
	ldw v0, GSA(t0)	
	ldw t0, 0(sp)
	addi sp, sp, 4
    ret
;END:get_gsa


;BEGIN:draw_gsa
draw_gsa:
	addi sp, sp, -32
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw t2, 8(sp)
	stw t3, 12(sp)
	stw ra, 16(sp)
	stw v0, 20(sp)
	stw a0, 24(sp)
	stw a1, 28(sp)
	addi t0, zero, 12 ; borne x
	addi t1, zero, 8 ; borne y
	add t2, zero, zero ; compteur x
	add t3, zero, zero ; compteur y
	beq t2, t0, 36	;it�re 12 fois sur x
	slli a0, t2, 3 ; registre pour x * 8

	beq t3, t1, 16	;it�re 8 fois sur y
	add a1, zero, t3
	call helper_draw_gsa
	addi t3, t3, 1
	br -20 ;go back to condition on y
	
	addi t2, t2, 1
	add t3, zero, zero
	br -40 ; go back to condition on x
	

	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	ldw t3, 12(sp)
	ldw ra, 16(sp)
	ldw v0, 20(sp)
	ldw a0, 24(sp)
	ldw a1, 28(sp)
	addi sp, sp, 32
	ret
;END:draw_gsa



;BEGIN:helper
helper_draw_gsa:
	addi sp, sp, -8
	stw ra, 0(sp)
	stw a0, 4(sp)
	srli a0, a0, 3
    call get_gsa
    beq v0, zero, 20
    call set_pixel	;case of should be lighten
	ldw ra, 0(sp)
	ldw a0, 4(sp)
	addi sp, sp, 8
    ret
    call helper_set_pixel_to_0	; case of should be black
	ldw ra, 0(sp)
	ldw a0, 4(sp)
	addi sp, sp, 8
    ret
;END:helper

;BEGIN:helper
helper_set_pixel_to_0:
	addi sp, sp, -24
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw t2, 8(sp)
	stw t3, 12(sp)
	stw t4, 16(sp)
	stw t5, 20(sp)
    cmpgeui t0, a0, 8 ;check si >= 8
    cmpgeui t1, a0, 4 ;check si >= 4
    addi t2, zero, 1 ;cte 1
    beq t0, t2, 132
    beq t1, t2, 60
;Cas 1    
    ldw t4, 0x2000(zero)
    slli t3, a0, 3
    add t3, t3, a1
    sll t5, t2, t3
    nor t5, t5, zero
    and t4, t4, t5
    stw t4, 0x2000(zero)
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	ldw t3, 12(sp)
	ldw t4, 16(sp)
	ldw t5, 20(sp)
	addi sp, sp, 24
    ret
;Cas 2
    ldw t4, 0x2004(zero)
    addi t5, zero, 4 ; cte = 4
    sub t3, a0, t5
    slli t3, t3, 3
    add t3, t3, a1
    sll t5, t2, t3
    nor t5, t5, zero
    and t4, t4, t5
    stw t4, 0x2004(zero)
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	ldw t3, 12(sp)
	ldw t4, 16(sp)
	ldw t5, 20(sp)
	addi sp, sp, 24
    ret
; Cas3
    ldw t4, 0x2008(zero)
    addi t5, zero, 8 ; cte = 8
    sub t3, a0, t5
    slli t3, t3, 3
    add t3, t3, a1
    sll t5, t2, t3
    nor t5, t5, zero
    and t4, t4, t5
    stw t4, 0x2008(zero)
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	ldw t3, 12(sp)
	ldw t4, 16(sp)
	ldw t5, 20(sp)
	addi sp, sp, 24
    ret
;END:helper

;BEGIN:draw_tetromino
draw_tetromino:
	addi sp, sp, -40
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw a1, 8(sp)
	stw a2, 12(sp)
	stw ra, 16(sp)
	stw t2, 20(sp)
	stw t3, 24(sp)
	stw a0, 28(sp)
	stw v0, 32(sp)
	stw v1, 36(sp)
	call helper_get_pos_matrix
	add a2, zero, a0	
	ldw a0, T_X(zero)
	ldw a1, T_Y(zero)
	add t2, zero, a0
	add t3, zero, a1
	call set_gsa ;set for anchor point
	ldw t0, 0(v0)
	ldw t1, 0(v1)
	add a0, t2, t0
	add a1, t3, t1
	call set_gsa ;set for first offset
	ldw t0, 4(v0)
	ldw t1, 4(v1)
	add a0, t2, t0
	add a1, t3, t1
	call set_gsa ;set for second offset	
	ldw t0, 8(v0)
	ldw t1, 8(v1)
	add a0, t2, t0
	add a1, t3, t1
	call set_gsa ;set for third offset
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw a1, 8(sp)
	ldw a2, 12(sp)
	ldw ra, 16(sp)
	ldw t2, 20(sp)
	ldw t3, 24(sp)
	ldw a0, 28(sp)
	ldw v0, 32(sp)
	ldw v1, 36(sp)
	addi sp, sp, 40
	ret
;END:draw_tetromino



;BEGIN:generate_tetromino
generate_tetromino:
	addi sp, sp, -12
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw ra, 8(sp)
	ldw t0, RANDOM_NUM(zero)
	andi t0, t0, 7
	addi t1, zero, 5
	addi sp, sp, 12
	bge t0, t1, generate_tetromino
	stw t0, T_type(zero)
	add t0, zero, zero
	stw t0, T_orientation(zero)
	addi t0, zero, 6
	addi t1, zero, 1
	stw t0, T_X(zero)
	stw t1, T_Y(zero)
	addi sp, sp, -12
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw ra, 8(sp)
	addi sp, sp, 12
	ret
;END:generate_tetromino


;BEGIN:detect_collision:
detect_collision:
	addi sp, sp, -24
	stw a0, 0(sp)
	stw a1, 4(sp)
	stw a2, 8(sp)
	stw a3, 12(sp)
	stw t0, 16(sp)
	stw ra, 20(sp)

	addi t0, zero, OVERLAP
	beq a0, t0, 28	;jump is case overlap
	call helper_pos_after_moved ; case no overlap but for other coll
	add a0, a1, zero
	add a1, zero, a2
	add a2, zero, v0
	add a3, zero, v1
	call helper_find_overlap
	br 24

	call helper_get_pos_matrix	;case for overlap
	ldw a0, T_X(zero)
	ldw a1, T_Y(zero)
	add a2, zero, v0
	add a3, zero, v1
	call helper_find_overlap
	bne v0, zero, 36
	addi v0, zero, NONE	; case for v0 = NONE
	ldw a0, 0(sp)
	ldw a1, 4(sp)
	ldw a2, 8(sp)
	ldw a3, 12(sp)
	ldw t0, 16(sp)
	ldw ra, 20(sp)
	addi sp, sp, 24
	ret

	ldw a0, 0(sp)
	ldw a1, 4(sp)
	ldw a2, 8(sp)
	ldw a3, 12(sp)
	ldw t0, 16(sp)
	ldw ra, 20(sp)
	addi sp, sp, 24
	add v0, zero, a0 ;	case for v0 = collision
	ret
;END:detect_collision:

;BEGIN:helper
helper_get_pos_matrix:
	ldw s0, T_type(zero)
	ldw s1, T_orientation(zero)
	slli s0, s0, 2
	add s1, s0, s1
	slli s1, s1, 2
	ldw v0, DRAW_Ax(s1)
	ldw v1, DRAW_Ay(s1)
	ret
;END:helper

;BEGIN:helper
helper_pos_after_moved:
	addi sp, sp, -16
	stw t3, 0(sp)
	stw ra, 4(sp)
	stw t1, 8(sp)
	stw t2, 12(sp)
	ldw t1, T_X(zero)
	ldw t2, T_Y(zero)
	addi t3, zero, W_COL
	bne a0, t3, 36
	addi a1, t1, -1
	add a2, t2, zero
	call helper_get_pos_matrix
	ldw t3, 0(sp)
	ldw ra, 4(sp)
	ldw t1, 8(sp)
	ldw t2, 12(sp)
	addi sp, sp, 16
	ret
	addi t3, zero, E_COL
	bne a0, t3, 36
	addi a1, t1, 1
	add a2, t2, zero
	call helper_get_pos_matrix
	ldw t3, 0(sp)
	ldw ra, 4(sp)
	ldw t1, 8(sp)
	ldw t2, 12(sp)
	addi sp, sp, 16
	ret
	add a1, t1, zero
	addi a2, t2, 1
	call helper_get_pos_matrix
	ldw t3, 0(sp)
	ldw ra, 4(sp)
	ldw t1, 8(sp)
	ldw t2, 12(sp)
	addi sp, sp, 16
	ret
;END:helper

;BEGIN:in_gsa
in_gsa:
	addi sp, sp, -24
	stw t1, 0(sp)
	stw t2, 4(sp)
	stw t3, 8(sp)
	stw t4, 12(sp)
	stw t5, 16(sp)
	stw t6, 20(sp)
	addi t5, zero, 11
	addi t6, zero, 7
	cmplti t1, a0, 0
	cmplt t2, t5, a0
	cmplti t3, a1, 0
	cmplt t4, t6, a1
	or t2, t1, t2
	or t4, t3, t4
	or t5, t2, t4
	add v0, zero, t5
	ldw t1, 0(sp)
	ldw t2, 4(sp)
	ldw t3, 8(sp)
	ldw t4, 12(sp)
	ldw t5, 16(sp)
	ldw t6, 20(sp)
	addi sp, sp, 24
    ret
;END:in_gsa

;BEGIN:helper
helper_find_overlap:
	addi sp, sp, -28
	stw ra, 0(sp)
	stw t0, 4(sp)
	stw t1, 8(sp)
	stw t2, 12(sp)
	stw t3, 16(sp)
	stw a0, 20(sp)
	stw a1, 24(sp)
	
	add t2, zero, a0
	add t3, zero, a1

	call in_gsa
	bne v0, zero, 108
	call get_gsa
	bne v0, zero, 100 ; check case if overlap in anchorpoint
	
	ldw t0, 0(a2)
	ldw t1, 0(a3)
	add a0, t2, t0
	add a1, t3, t1
	call get_gsa
	bne v0, zero, 76 ; check case if overlap in offset #1
	call in_gsa
	bne v0, zero, 68
	
	ldw t0, 4(a2)
	ldw t1, 4(a3)
	add a0, t2, t0
	add a1, t3, t1
	call get_gsa
	bne v0, zero, 44 ; check case if overlap in offset #2
	call in_gsa
	bne v0, zero, 36

	ldw t0, 8(a2)
	ldw t1, 8(a3)
	add a0, t2, t0
	add a1, t3, t1
	call get_gsa 
	bne v0, zero, 12 ; check case if overlap in offset #3
	call in_gsa
	bne v0, zero, 4
	
	add v0, zero, zero ; to be sure that if no collision or v0 = 0


	ldw ra, 0(sp)
	ldw t0, 4(sp)
	ldw t1, 8(sp)
	ldw t2, 12(sp)
	ldw t3, 16(sp)
	ldw a0, 20(sp)
	ldw a1, 24(sp)
	addi sp, sp, 28
	ret
;END:helper	
	

;BEGIN:increment_score
increment_score:
	addi sp, sp, -8
	stw t0, 0(sp)
	stw t1, 4(sp)
	ldw t0, SCORE(zero)
	addi t1, zero, 999
	blt t0, t1, 8
	stw zero, SCORE(zero)
	br 8
	addi t0, t0, 1
	stw t0, SCORE(zero)
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	addi sp, sp, 8
	ret
;END:increment_score

;BEGIN:display_score
display_score:
	addi sp, sp, -36
    stw t0, 0(sp)
    stw t1, 4(sp)
    stw t2, 8(sp)
    stw t3, 12(sp)
    stw t4, 16(sp)
    stw a0, 20(sp)
    stw ra, 24(sp)
    stw v0, 28(sp)
    stw v1, 32(sp)

    ldw t0, SCORE(zero) ;t0 = score
    add a0, zero, t0
    addi a1, zero, 1000
	call helper_get_score
	add t1, v0, zero ; thousands
	add a0, v1, zero
	addi a1, zero, 100
	call helper_get_score
	add t2, v0, zero ;hundreds
	add a0, v1, zero
	addi a1, zero, 10
	call helper_get_score
	add t3, v0, zero ; decades
	add t4, v1, zero; units
    
	slli t1, t1, 2
	ldw t0, font_data(t1)
	stw t0, SEVEN_SEGS(zero)

	slli t2, t2, 2
	ldw t0, font_data(t2)
	addi t1, zero, 4
	stw t0, SEVEN_SEGS(t1)

	slli t3, t3, 2
	ldw t0, font_data(t3)
	addi t1, t1, 4
	stw t0, SEVEN_SEGS(t1)

	slli t4, t4, 2
	ldw t0, font_data(t4)
	addi t1, t1, 4
	stw t0, SEVEN_SEGS(t1)	

    ldw t0, 0(sp)
    ldw t1, 4(sp)
    ldw t2, 8(sp)
    ldw t3, 12(sp)
    ldw t4, 16(sp)
    ldw a0, 20(sp)
    ldw ra, 24(sp)
    ldw v0, 28(sp)
    ldw v1, 32(sp)
    addi sp, sp, 36
    ret
;END:display_score



;BEGIN:helper
helper_get_score:
	add v0, zero, zero
	blt a0, zero, 12
	sub a0, a0, a1
	addi v0, v0, 1
	br -16
	addi v0, v0, -1
	add v1, a0, a1
	ret
;END:helper




;BEGIN:rotate_tetromino
rotate_tetromino:
	addi sp, sp, -4
	stw t0, 0(sp)
	addi t0, zero, rotL
	bne a0, t0, 28
	ldw t0, T_orientation(zero)
	addi t0, t0, -1
	andi t0, t0, 3
	stw t0, T_orientation(zero)
	ldw t0, 0(sp)
	addi sp, sp, 4
	ret
	ldw t0, T_orientation(zero)
	addi t0, t0, 1
	andi t0, t0, 3
	stw t0, T_orientation(zero)
	ldw t0, 0(sp)
	addi sp, sp, 4
	ret
;END:rotate_tetromino

;BEGIN:act
act:
	addi sp, sp, -20
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw a0, 8(sp)
	stw ra, 12(sp)
	stw t2, 16(sp)
	addi t2, zero, NONE
	add t1, zero, a0 ; t1 = a0

	addi t0, zero, moveL ; t0 = moveL
	bne t1, t0, 32 				;case if moving left
	addi a0, zero, W_COL
	call detect_collision 
	beq v0, t2, 4	
	br 332	; if collision
	

	ldw t0, T_X(zero) 	;if no collision, move 
	addi t0, t0, -1	
	stw t0, T_X(zero)
	br 324; no collision

	addi t0, zero, moveR ; case of moving right
	bne t1, t0, 32 
	addi a0, zero, E_COL
	call detect_collision 
	beq v0, t2, 4	
	br 292	; if collision
	

	ldw t0, T_X(zero) 	;if no collision, move 
	addi t0, t0, 1	
	stw t0, T_X(zero)
	br 284;no collision

	addi t0, zero, moveD ; case of moving down
	bne t1, t0, 32 
	addi a0, zero, So_COL
	call detect_collision 
	beq v0, t2, 4	
	br 252	; if collision
	

	ldw t0, T_Y(zero) 	;if no collision, move 
	addi t0, t0, 1
	stw t0, T_Y(zero)
	br 244;no collision
	
	addi t0, zero, reset ; case of reset
	bne t1, t0, 8
	call reset_game
	br 228;no collision
	
	addi t0, zero, rotL ;case of rotL
	bne t1, t0, 104
	add a0, zero, t1
	call rotate_tetromino
	addi a0, zero, OVERLAP ; is there a collision?
	call detect_collision	;...
	beq v0, a0, 4  ;...
	br	196;if no collision
	

	ldw t0, T_X(zero) 	;move one time to right
	addi t0, t0, 1	
	stw t0, T_X(zero)
	call detect_collision
	beq v0, a0, 4
	br	172;if no collision after have been moved one time


	ldw t0, T_X(zero) 	;move another time (2) to the right
	addi t0, t0, 1	
	stw t0, T_X(zero)
	call detect_collision
	beq v0, a0, 4
	br 148	;if no collision after have been moved two times


	ldw t0, T_X(zero) 	;if still collisions, go back to normal
	addi t0, t0, -2	
	stw t0, T_X(zero)
	ldw t0, T_orientation(zero)	;set back position and orientation to normal
	addi t0, t0, 1
	andi t0, t0, 3
	stw t0, T_orientation(zero)
	br 108; cas collision


	addi t0, zero, rotR ;case of rotR
	bne t1, t0, 20
	add a0, zero, t1
	call rotate_tetromino
	addi a0, zero, OVERLAP ; is there a collision?
	call detect_collision	;...
	beq v0, a0, 4  ;...
	br	84;if no collision
	

	ldw t0, T_X(zero) 	;move one time to left
	addi t0, t0, -1	
	stw t0, T_X(zero)
	call detect_collision
	beq v0, a0, 4

	br	60;if no collision after have been moved one time

	ldw t0, T_X(zero) 	;move another time (2) to the left
	addi t0, t0, -1	
	stw t0, T_X(zero)
	call detect_collision
	beq v0, a0, 4
	br 36; cas pas collision

	ldw t0, T_X(zero) 	;if still collisions, go back to normal
	addi t0, t0, 2	
	stw t0, T_X(zero)
	ldw t0, T_orientation(zero)	;set back position and orientation to normal
	addi t0, t0, 3
	andi t0, t0, 3
	stw t0, T_orientation(zero)



	addi v0, zero, 1	;jump si collision
	br 4
	add v0, zero, zero ; jump si pas collision
	ldw t0, 0(sp)	
	ldw t1, 4(sp)
	ldw a0, 8(sp)
	ldw ra, 12(sp)
	ldw t2, 16(sp)
	addi sp, sp, 20
	ret
;END:act

;BEGIN:reset_game
reset_game:
	addi sp, sp, -28
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw t2, 8(sp)
	stw a0, 12(sp)
	stw a1, 16(sp)
	stw a2, 20(sp)
	stw ra, 24(sp)

	stw zero, SCORE(zero)
	call generate_tetromino
	;call display_score
	addi t0, zero, 12
	addi t1, zero, 8
	add a0, zero, zero
	add a1, zero, zero
	addi a2, zero, NOTHING

	beq a0, t0, 28
	beq a1, t1, 12
	call set_gsa
	addi a1, a1, 1
	br -16
	add a1, zero, zero
	addi a0, a0, 1
	br -32
	
	addi a0, zero, FALLING
	call draw_tetromino
	call draw_gsa
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	ldw a0, 12(sp)
	ldw a1, 16(sp)
	ldw a2, 20(sp)
	ldw ra, 24(sp)
	addi sp, sp, 28
	ret
;END:reset_game


;BEGIN:detect_full_line
detect_full_line:
	addi sp, sp, -20
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw a0, 8(sp)
	stw a1, 12(sp)
	stw ra, 16(sp)
	addi a1, zero, 7
	addi t0, zero, 1 
	addi t1, zero, 11


loopa: ;d'abord on essaie de trouver une ligne dont l'élément le plus à gauche est à 1
	add a0, zero, zero ;il faut réinitialiser le compteur des x à chaque fois
	call get_gsa
	beq v0, t0, 12			 ;loopb
	beq a1, zero, 28 		 ;no_full_line_case
	sub a1, a1, t0
	br -24					 ;loopa

loopb: ;ensuite on teste tous les éléments de la ligne trouvée dans loop1
	bge a0, t1, 48 			 ;one_full_line_case
	add a0, a0, t0
	call get_gsa
	beq v0, t0, -16			 ;loopb
	br -32 					 ;retourne sur loopa
	

no_full_line_case:
	addi v0, zero, 8
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw a0, 8(sp)
	ldw a1, 12(sp)
	ldw ra, 16(sp)
	addi sp, sp, 20
	ret

one_full_line_case:
	add v0, zero, a1
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw a0, 8(sp)
	ldw a1, 12(sp)
	ldw ra, 16(sp)
	addi sp, sp, 20
	ret

;END:detect_full_line


;BEGIN:remove_full_line
remove_full_line:

	addi sp, sp, -32
	stw t0, 0(sp)
	stw t1, 4(sp)
	stw t2, 8(sp)
	stw a0, 12(sp)
	stw a1, 16(sp)
	stw a2, 20(sp)
	stw ra, 24(sp)
	stw t3, 28(sp)

	add a1, zero, a0
	add t1, zero, a0
	addi t2, zero, 12
	addi t3, zero, 8
	add a0, zero, zero
	addi a2, zero, NOTHING ;comment charger une constante?
	addi t0, zero, 1 

main_prog:
	call helper_loop
	call draw_gsa
	call wait

	addi a2, zero, PLACED
	add a0, zero, zero
	call helper_loop
	call draw_gsa
	call wait

	addi a2, zero, NOTHING
	add a0, zero, zero
	call helper_loop 
	call draw_gsa
	call wait

	addi a2, zero, PLACED
	add a0, zero, zero
	call helper_loop 
	call draw_gsa
	call wait

	addi a2, zero, NOTHING
	add a0, zero, zero
	call helper_loop 
	call draw_gsa

	add a0, zero, zero
	br 64 		;moving_down_the_lines
	ldw t0, 0(sp)
	ldw t1, 4(sp)
	ldw t2, 8(sp)
	ldw a0, 12(sp)
	ldw a1, 16(sp)
	ldw a2, 20(sp)
	ldw ra, 24(sp)
	ldw t3, 28(sp)
	addi sp, sp, 32
	ret ;return final de la fonction

	
moving_down_inner_loop: ;on descend toutes les cases d'une colonne (une colonne à la fois)
	call get_gsa
	call set_gsa
	add a1, a1, t0
	add a2, zero, v0
	bne a1, t3, -20			;moving_down_inner_loop
	;call set_gsa ;ATTENTION
	br 12	;retourne dans moving_down_the_lines

moving_down_the_lines: ;on parcourt les différentes colonnes
	add a1, zero, zero
	addi a2, zero, NOTHING
	br -36					;moving_down_inner_loop
	add a0, a0, t0
	bne a0, t2, -20 		;moving_down_the_lines
	br -84					;retourne dans le main
	
;END:remove_full_line


;BEGIN:helper
helper_loop:
	addi sp, sp, -4
	stw ra, 0(sp)
	call set_gsa
	add a0, a0, t0
	bne a0, t2, -12  		;loop
	ldw ra, 0(sp)
	addi sp, sp, 4
	ret
;END:helper


font_data:
    .word 0xFC  ; 0
    .word 0x60  ; 1
    .word 0xDA  ; 2
    .word 0xF2  ; 3
    .word 0x66  ; 4
    .word 0xB6  ; 5
    .word 0xBE  ; 6
    .word 0xE0  ; 7
    .word 0xFE  ; 8
    .word 0xF6  ; 9

C_N_X:
  .word 0x00
  .word 0xFFFFFFFF
  .word 0xFFFFFFFF

C_N_Y:
  .word 0xFFFFFFFF
  .word 0x00
  .word 0xFFFFFFFF

C_E_X:
  .word 0x01
  .word 0x00
  .word 0x01

C_E_Y:
  .word 0x00
  .word 0xFFFFFFFF
  .word 0xFFFFFFFF

C_So_X:
  .word 0x01
  .word 0x00
  .word 0x01

C_So_Y:
  .word 0x00
  .word 0x01
  .word 0x01

C_W_X:
  .word 0xFFFFFFFF
  .word 0x00
  .word 0xFFFFFFFF

C_W_Y:
  .word 0x00
  .word 0x01
  .word 0x01

B_N_X:
  .word 0xFFFFFFFF
  .word 0x01
  .word 0x02

B_N_Y:
  .word 0x00
  .word 0x00
  .word 0x00

B_E_X:
  .word 0x00
  .word 0x00
  .word 0x00

B_E_Y:
  .word 0xFFFFFFFF
  .word 0x01
  .word 0x02

B_So_X:
  .word 0xFFFFFFFE
  .word 0xFFFFFFFF
  .word 0x01

B_So_Y:
  .word 0x00
  .word 0x00
  .word 0x00

B_W_X:
  .word 0x00
  .word 0x00
  .word 0x00

B_W_Y:
  .word 0xFFFFFFFE
  .word 0xFFFFFFFF
  .word 0x01

T_N_X:
  .word 0xFFFFFFFF
  .word 0x00
  .word 0x01

T_N_Y:
  .word 0x00
  .word 0xFFFFFFFF
  .word 0x00

T_E_X:
  .word 0x00
  .word 0x01
  .word 0x00

T_E_Y:
  .word 0xFFFFFFFF
  .word 0x00
  .word 0x01

T_So_X:
  .word 0xFFFFFFFF
  .word 0x00
  .word 0x01

T_So_Y:
  .word 0x00
  .word 0x01
  .word 0x00

T_W_X:
  .word 0x00
  .word 0xFFFFFFFF
  .word 0x00

T_W_Y:
  .word 0xFFFFFFFF
  .word 0x00
  .word 0x01

S_N_X:
  .word 0xFFFFFFFF
  .word 0x00
  .word 0x01

S_N_Y:
  .word 0x00
  .word 0xFFFFFFFF
  .word 0xFFFFFFFF

S_E_X:
  .word 0x00
  .word 0x01
  .word 0x01

S_E_Y:
  .word 0xFFFFFFFF
  .word 0x00
  .word 0x01

S_So_X:
  .word 0x01
  .word 0x00
  .word 0xFFFFFFFF

S_So_Y:
  .word 0x00
  .word 0x01
  .word 0x01

S_W_X:
  .word 0x00
  .word 0xFFFFFFFF
  .word 0xFFFFFFFF

S_W_Y:
  .word 0x01
  .word 0x00
  .word 0xFFFFFFFF

L_N_X:
  .word 0xFFFFFFFF
  .word 0x01
  .word 0x01

L_N_Y:
  .word 0x00
  .word 0x00
  .word 0xFFFFFFFF

L_E_X:
  .word 0x00
  .word 0x00
  .word 0x01

L_E_Y:
  .word 0xFFFFFFFF
  .word 0x01
  .word 0x01

L_So_X:
  .word 0xFFFFFFFF
  .word 0x01
  .word 0xFFFFFFFF

L_So_Y:
  .word 0x00
  .word 0x00
  .word 0x01

L_W_X:
  .word 0x00
  .word 0x00
  .word 0xFFFFFFFF

L_W_Y:
  .word 0x01
  .word 0xFFFFFFFF
  .word 0xFFFFFFFF

DRAW_Ax:                        ; address of shape arrays, x axis
    .word C_N_X
    .word C_E_X
    .word C_So_X
    .word C_W_X
    .word B_N_X
    .word B_E_X
    .word B_So_X
    .word B_W_X
    .word T_N_X
    .word T_E_X
    .word T_So_X
    .word T_W_X
    .word S_N_X
    .word S_E_X
    .word S_So_X
    .word S_W_X
    .word L_N_X
    .word L_E_X
    .word L_So_X
    .word L_W_X

DRAW_Ay:                        ; address of shape arrays, y_axis
    .word C_N_Y
    .word C_E_Y
    .word C_So_Y
    .word C_W_Y
    .word B_N_Y
    .word B_E_Y
    .word B_So_Y
    .word B_W_Y
    .word T_N_Y
    .word T_E_Y
    .word T_So_Y
    .word T_W_Y
    .word S_N_Y
    .word S_E_Y
    .word S_So_Y
    .word S_W_Y
    .word L_N_Y
    .word L_E_Y
    .word L_So_Y
    .word L_W_Y